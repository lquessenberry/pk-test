# PK Drupal Test

Initially I had some issues getting Drupal 8.3.3 to render for the first time after the guided install via my browser. I didn't want to waste my time troubleshooting a Drupal Core or Composer issue so I tried using the Drush site install command instead and got the website to install. We can check into this issue later if it continues to arise.

### Menu Hierarchy
To start, I read over the documentation and viewed the Adobe XD wireframe with organization in mind. I noticed a parent/child thing going on with the side menus, so I went for the SHS (Simple Hierarchichal Select) module and established a hierarchical relationship between each menu item. They seemed like increasingly granular descriptors for the OAS spec so I created them as taxonomy terms and ordered them hierarchically. This was tricky and I may have gone with a more familiar route of jsut using the Drupal menu system to organize this. Nevertheless it does seem to work with some reservations.

### Category Terms
The categories are actually a different vocabulary than the rest of the terms and I made them a field that is part of the "Api Package" level of taxonomy terms. This allowed me to create a view that I called Tags by Category which was used to populate the "API Packages" main page."

### API Packages Page (Page A)
Nothing fancy here. This is a basic page with a field for an image banner. I thin I forgot to put a custom text block in there, but I know how. I used Layout Builder with some Bootstrap prebuilt layouts to organize the blocks on the page. Layout is used on the terms and pages so the end user has customization options immediately, and for one off changes.

### API Package 1 (Page B)
Nothing fancy here, but I did use a taxonomy term as the bundle/entity rather than a node. This allows us to define the packages and assign those definitions to the OAS Spec pages once instead of writing every part one at a time. I included a basic HTML editor for the description and made some ugly WYSIWYG content. 

### OpenAPI Specifications (Page C1 and C2)
This varies slightly on my version, but the overall gist should be there. I basically made a view that lists the child terms of the parent term. In this case the OAS1 term and OAS 2 term are children of API Package 1 which is also a term. A simple view with the rendered title and summary was used to populate the results.

Note: I didn't get around to populating enough content to make OAS 3 and OAS 4, but the functionality is there. It will populate appropriately if you fill out the fields for those two terms.

### OAS 1 (Page D)
Call me thrifty, or maybe pressed for time, but I used the contrib swagger_ui_formatter module for the file field. This was simple enough to upload the OAS yaml file and have it render. If this isn't best practice, I am willing to learn.

## Conclusion

I used several modules and even some that I ended up not needing. I got a bit in the weeds trying to get the grids to work on some of the layouts so I used a Bootstrap collection of layouts to make it work. I know there are other ways to organize the hierarchy and relations between the specs and the groups and categories they belong in. My approach was to make it simple to input the OAS (Swagger Pages) last after describing the categories and packages they belong to. The end user would just select the proper hierarchy for each Swagger page to fall into and the views would do the rest. I do realize that I did some workarounds including using views and placing custom style in views that are probably not best practice. If I had more time I would have written custom layouts and views templates in Twig or not used views at all and written some functions to order entities using Drupal's Entity API. 

I look forward to your review.